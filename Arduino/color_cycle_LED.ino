void setup() {
  #define RED 2
  #define GREEN 3
  #define BLUE 4
  
  
  pinMode(RED, OUTPUT);
  pinMode(BLUE, OUTPUT);
  pinMode(GREEN, OUTPUT);
}

void loop() {  

   //green//
  digitalWrite(GREEN, HIGH);
  delay(300);
  digitalWrite(GREEN, LOW);
  delay(150);
  
  // Neon green //
  digitalWrite(RED, HIGH);
  digitalWrite(GREEN, HIGH);
  delay(300);
  digitalWrite(GREEN, LOW);
  digitalWrite(RED, LOW);
  delay(150);
  
  //red//                  
  digitalWrite(RED, HIGH);
  delay(300);
  digitalWrite(RED, LOW);
  delay(150);
  
  //purple//
  digitalWrite(RED, HIGH);
  digitalWrite(BLUE, HIGH);
  delay(300);
  digitalWrite(BLUE, LOW);
  digitalWrite(RED, LOW);
  delay(150);
  
  //blue//
  digitalWrite(BLUE, HIGH);
  delay(300);
  digitalWrite(BLUE, LOW);
  delay(150);
 
  
  //teal//
  digitalWrite(BLUE, HIGH);
  digitalWrite(GREEN, HIGH);
  delay(300);
  digitalWrite(BLUE, LOW);
  digitalWrite(GREEN, LOW);
  delay(150);
  
  //white//
  digitalWrite(BLUE, HIGH);
  digitalWrite(GREEN, HIGH);
  digitalWrite(RED, HIGH);
  delay(300);
  digitalWrite(BLUE, LOW);
  digitalWrite(GREEN, LOW);
  digitalWrite(RED, LOW);
  delay(150);
  
}
  
